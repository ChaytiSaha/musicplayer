import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';

void main() {
  return runApp(
    PlayMusic(),
  );
}

class PlayMusic extends StatelessWidget {
  void playSound(int soundNumber) {
    final audioCache = AudioCache();
    audioCache.play('sounds/note${soundNumber.toString()}.wav');
  }

  Expanded buildWidget({int soundNumber, Color color}) {
    return Expanded(
      child: Container(
        color: color,
        child: TextButton(
          child: Text(
            "$soundNumber",
            style: TextStyle(color: Colors.white, fontSize: 24),
          ),
          onPressed: () {
            playSound(soundNumber);
          },
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              buildWidget(soundNumber: 1, color: Colors.red),
              buildWidget(soundNumber: 2, color: Colors.blue),
              buildWidget(soundNumber: 3, color: Colors.greenAccent),
              buildWidget(soundNumber: 4, color: Colors.green),
              buildWidget(soundNumber: 5, color: Colors.teal),
              buildWidget(soundNumber: 6, color: Colors.grey),
              buildWidget(soundNumber: 7, color: Colors.lightBlue),
            ],
          ),
        ),
      ),
    );
  }
}
